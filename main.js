const PizZip = require("pizzip")
const DocxTemplater = require("docxtemplater")
const express = require("express")

const app = express()

app.get("/status", (_, res) => res.status(200).json({
    ok: true,
}))

app.use(express.json({limit: "1GB"}))

app.post("/fill", (req, res) => {
    try {
        const {content, data} = req.body || {}
        if(!data || !content) throw new SyntaxError("incoming data must contain 'data' and 'content' properties")

        const output = fillTemplate(Buffer.from(content, "base64"), data)

        res.status(200).json({ok: true, content: output.toString("base64")})
    }catch(e) {
        res.status(e instanceof SyntaxError ? 400 : 500).json({ok: false, message: e.message, error: e})
    }
})

app.post("/echo", (req, res) => res.status(200).json(req.body))

function fillTemplate(buffer, data) {
    const zipfile = new PizZip(buffer)
    const template = new DocxTemplater(zipfile, {
        paragraphLoop: true,
        linebreaks: true,
        nullGetter: () => "",
    })

    template.render(data)

    return template.getZip().generate({
        type: "nodebuffer",
    })
}

const port = process.env.PORT || 8080

console.info(`Listening on port ${port}`)
app.listen(port)
