FROM docker.io/node:alpine

ENV NODE_ENV=production

RUN mkdir -p /app/
ADD *.js *.json /app/
WORKDIR /app

RUN npm install

CMD ["npm", "start"]
